# Ubuntu 16.04 based, runs as rundeck user
# https://hub.docker.com/r/rundeck/rundeck/tags
FROM rundeck/rundeck:3.4.5

MAINTAINER Matthew Williams <matthew.l.williams@cgi.com>

ENV ANSIBLE_HOST_KEY_CHECKING=false
ENV PATH=${PATH}:${RDECK_BASE}/tools/bin
ENV PATH=/home/rundeck/tools/python3/bin:${PATH}

ARG ANSIBLE_VERSION=2.9.26

HEALTHCHECK --interval=1m --timeout=5s --start-period=60s --retries=3 CMD curl -f http://localhost:4440/health || exit 1

#  mkdir -p /etc/ansible \
#  ${PROJECT_BASE}/acls \

# install ansible
# base image already installed: curl, openjdk-8-jdk-headless, ssh-client, sudo, uuid-runtime, wget
# (see https://github.com/rundeck/rundeck/blob/master/docker/ubuntu-base/Dockerfile)
# TODO: Install Kerberos
# TODO: Change version of python
RUN sudo apt-get -y update \
    && sudo DEBIAN_FRONTEND=noninteractive apt-get -y --no-install-recommends install ca-certificates python3-pip sshpass git gcc libpython3.6-dev python3.6-dev libkrb5-dev krb5-user \
    && sudo -H pip3 --no-cache-dir install --upgrade pip setuptools \
    && sudo -H pip3 --no-cache-dir install ansible==${ANSIBLE_VERSION} pywinrm[kerberos] \
    && sudo rm -rf /var/lib/apt/lists/*

# If we do this and then mount a venv somewhere all Rundeck can use we could upgrade things without taking down Rundeck
# How ever this goes against the use of containers I feel
#  && sudo apt-get install -y --no-install-recommends libpython3.7
